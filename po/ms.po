# Malay translation for gnome-contacts.
# Copyright (C) 2020 gnome-contacts's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-contacts package.
# abuyop <abuyop@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-contacts master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-contacts/issues\n"
"POT-Creation-Date: 2020-01-11 05:55+0000\n"
"PO-Revision-Date: 2020-01-27 17:27+0800\n"
"Language-Team: Pasukan Terjemahan GNOME Malaysia\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"Last-Translator: abuyop <abuyop@gmail.com>\n"
"X-Generator: Poedit 2.0.6\n"

#: data/org.gnome.Contacts.appdata.xml.in.in:6
#: data/org.gnome.Contacts.desktop.in.in:3 data/ui/contacts-window.ui:102
#: data/ui/contacts-window.ui:120 src/contacts-window.vala:224
msgid "Contacts"
msgstr "Hubungan"

#: data/org.gnome.Contacts.appdata.xml.in.in:7
#: data/org.gnome.Contacts.desktop.in.in:4
msgid "A contacts manager for GNOME"
msgstr "Sebuah pengurus hubungan untuk GNOME"

#: data/org.gnome.Contacts.appdata.xml.in.in:9
msgid ""
"Contacts keeps and organize your contacts information. You can create, edit, "
"delete and link together pieces of information about your contacts. Contacts "
"aggregates the details from all your sources providing a centralized place "
"for managing your contacts."
msgstr ""
"Hubungan menyimpan dan mengurus maklumat hubungan anda. Anda boleh cipta, "
"sunting, padam dan paut cebisan maklumat bersama-sama berkenaan hubungan "
"anda. Hubungan mengumpul perincian semua sumber anda dengan menjadi pusat "
"pengurusan hubungan anda."

#: data/org.gnome.Contacts.appdata.xml.in.in:15
msgid ""
"Contacts will also integrate with online address books and automatically "
"link contacts from different online sources."
msgstr ""
"Hubungan juga akan disepadukan dengan buku alamat dalam-talian dan pautkan "
"kenalan secara automatik dari sumber dalam-talian yang berlainan."

#: data/org.gnome.Contacts.appdata.xml.in.in:403
msgid "The GNOME Project"
msgstr "Projek GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Contacts.desktop.in.in:6
msgid "friends;address book;"
msgstr "rakan;buku alamat;"

#: data/gtk/help-overlay.ui:9
msgctxt "shortcut window"
msgid "Overview"
msgstr "Selayang Pandang"

#: data/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Help"
msgstr "Bantuan"

#: data/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Create a new contact"
msgstr "Cipta satu hubungan baharu"

#: data/gtk/help-overlay.ui:28
msgctxt "shortcut window"
msgid "Search"
msgstr "Cari"

#: data/gtk/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Shortcut list"
msgstr "Senarai pintasan"

#: data/gtk/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Quit"
msgstr "Keluar"

#: data/ui/contacts-avatar-selector.ui:54
msgid "Take a Picture…"
msgstr "Ambil satu Gambar…"

#: data/ui/contacts-avatar-selector.ui:68
msgid "Select a File…"
msgstr "Pilih satu Fail…"

#: data/ui/contacts-contact-editor.ui:8
msgid "Home email"
msgstr "Emel rumah"

#: data/ui/contacts-contact-editor.ui:12
msgid "Work email"
msgstr "Emel kerja"

#: data/ui/contacts-contact-editor.ui:16
msgid "Mobile phone"
msgstr "Telefon bimbit"

#: data/ui/contacts-contact-editor.ui:20
msgid "Home phone"
msgstr "Telefon rumah"

#: data/ui/contacts-contact-editor.ui:24
msgid "Work phone"
msgstr "Telefon tempat kerja"

#: data/ui/contacts-contact-editor.ui:28 src/contacts-contact-editor.vala:640
#: src/contacts-contact-editor.vala:647 src/contacts-contact-sheet.vala:234
msgid "Website"
msgstr "Laman Sesawang"

#: data/ui/contacts-contact-editor.ui:32 src/contacts-contact-editor.vala:666
#: src/contacts-contact-editor.vala:673 src/contacts-contact-sheet.vala:241
msgid "Nickname"
msgstr "Gelaran"

#: data/ui/contacts-contact-editor.ui:36 src/contacts-contact-editor.vala:699
#: src/contacts-contact-editor.vala:706 src/contacts-contact-sheet.vala:247
msgid "Birthday"
msgstr "Hari Lahir"

#: data/ui/contacts-contact-editor.ui:40
msgid "Home address"
msgstr "Alamat rumah"

#: data/ui/contacts-contact-editor.ui:44
msgid "Work address"
msgstr "Alamat tempat kerja"

#: data/ui/contacts-contact-editor.ui:48
msgid "Notes"
msgstr "Catatan"

#: data/ui/contacts-contact-editor.ui:72
msgid "New Detail"
msgstr "Perincian Baharu"

#: data/ui/contacts-contact-editor.ui:90
#: data/ui/contacts-linked-personas-dialog.ui:12
msgid "Linked Accounts"
msgstr "Akaun Terpaut"

#: data/ui/contacts-contact-editor.ui:96
msgid "Remove Contact"
msgstr "Buang Hubungan"

#: data/ui/contacts-contact-pane.ui:30
msgid "Select a contact"
msgstr "Pilih satu hubungan"

#: data/ui/contacts-crop-cheese-dialog.ui:27 data/ui/contacts-window.ui:171
#: data/ui/contacts-window.ui:235 src/contacts-app.vala:120
msgid "Cancel"
msgstr "Batal"

#: data/ui/contacts-crop-cheese-dialog.ui:49
msgid "Take Another…"
msgstr "Ambil Lain…"

#: data/ui/contacts-crop-cheese-dialog.ui:64 data/ui/contacts-window.ui:293
#: src/contacts-window.vala:231
msgid "Done"
msgstr "Selesai"

#: data/ui/contacts-linked-personas-dialog.ui:45
msgid "You can link contacts by selecting them from the contacts list"
msgstr "Anda boleh memautkan hubungan dengan memilihnya dari senarai hubungan"

#: data/ui/contacts-link-suggestion-grid.ui:56
msgid "Link Contacts"
msgstr "Paut Hubungan"

#: data/ui/contacts-list-pane.ui:22
msgid "Type to search"
msgstr "Taip untuk gelintar"

#. Link refers to the verb, from linking contacts together
#: data/ui/contacts-list-pane.ui:54
msgid "Link"
msgstr "Pautan"

#: data/ui/contacts-list-pane.ui:68
msgid "Remove"
msgstr "Buang"

#: data/ui/contacts-setup-window.ui:13
msgid "Contacts Setup"
msgstr "Persediaan Hubungan"

#: data/ui/contacts-setup-window.ui:22
msgid "_Quit"
msgstr "_keluar"

#: data/ui/contacts-setup-window.ui:27
msgid "Cancel setup and quit"
msgstr "Batal persediaan dan keluar"

#: data/ui/contacts-setup-window.ui:41
msgid "_Done"
msgstr "_Selesai"

#: data/ui/contacts-setup-window.ui:45
msgid "Setup complete"
msgstr "Persediaan selesai"

#: data/ui/contacts-setup-window.ui:86
msgid "Welcome"
msgstr "Selamat Datang"

#: data/ui/contacts-setup-window.ui:98
msgid ""
"Please select your main address book: this is where new contacts will be "
"added. If you keep your contacts in an online account, you can add them "
"using the online accounts settings."
msgstr ""
"Sila pilih buku alamat utama anda: merupakan tempat hubungan baharu akan "
"ditambah. Jika anda menyimpan hubungan anda melalui akaun dalam talian, anda "
"boleh menambahnya melalui tetapan akaun dalam talian."

#: data/ui/contacts-window.ui:15
msgid "List contacts by:"
msgstr "Senarai hubungan mengikut:"

#: data/ui/contacts-window.ui:28
msgid "First name"
msgstr "Nama pertama"

#: data/ui/contacts-window.ui:35
msgid "Surname"
msgstr "Nama keluarga"

#: data/ui/contacts-window.ui:49
msgid "Change Address Book…"
msgstr "Ubah Buku Alamat…"

#: data/ui/contacts-window.ui:57
msgid "Online Accounts <sup>↗</sup>"
msgstr "Akaun Dalam Talian <sup>↗</sup>"

#: data/ui/contacts-window.ui:73
msgid "Keyboard Shortcuts"
msgstr "Pintasan Papan Kekunci"

#: data/ui/contacts-window.ui:81
msgid "Help"
msgstr "Bantuan"

#: data/ui/contacts-window.ui:89
msgid "About Contacts"
msgstr "Perihal Hubungan"

#: data/ui/contacts-window.ui:128
msgid "Create new contact"
msgstr "Cipta hubungan baharu"

#: data/ui/contacts-window.ui:132
msgid "Add contact"
msgstr "Tambah hubungan"

#: data/ui/contacts-window.ui:172
msgid "Cancel selection"
msgstr "Batal pemilihan"

#: data/ui/contacts-window.ui:216
msgid "Back"
msgstr "Undur"

#: data/ui/contacts-window.ui:253
msgid "Edit details"
msgstr "Sunting perincian"

#: data/ui/contacts-window.ui:355
msgid "Loading"
msgstr "Memuatkan"

#: src/contacts-accounts-list.vala:124 src/contacts-esd-setup.vala:149
msgid "Local Address Book"
msgstr "Buku Alamat Setempat"

#: src/contacts-app.vala:43
msgid "Show contact with this email address"
msgstr "Tunjuk hubungan dengan alamat emel ini"

#: src/contacts-app.vala:44
msgid "Show contact with this individual id"
msgstr "Tunjuk hubungan dengan id individu ini"

#: src/contacts-app.vala:45
msgid "Show contacts with the given filter"
msgstr "Tunjuk hubungan dengan penapis yang diberi"

#: src/contacts-app.vala:46
msgid "Show the current version of Contacts"
msgstr "Tunjuk versi semasa Hubungan"

#: src/contacts-app.vala:107
#, c-format
msgid "No contact with id %s found"
msgstr "Tiada hubungan dengan id %s ditemui"

#: src/contacts-app.vala:108 src/contacts-app.vala:252
msgid "Contact not found"
msgstr "Hubungan tidak ditemui"

#: src/contacts-app.vala:118
msgid "Change Address Book"
msgstr "Ubah Buku Alamat"

#: src/contacts-app.vala:119
msgid "Change"
msgstr "Ubah"

#: src/contacts-app.vala:149
msgid ""
"New contacts will be added to the selected address book.\n"
"You are able to view and edit contacts from other address books."
msgstr ""
"Kenalan baharu akan ditambah ke dalam buku alamat terpilih.\n"
"Anda boleh lihat dan sunting hubungan dari buku alamat lain."

#: src/contacts-app.vala:232
msgid "translator-credits"
msgstr ""
"abuyop\n"
"Hasbullah Bin Pit https://launchpad.net/~sebol\n"
"dequan https://launchpad.net/~dequan"

#: src/contacts-app.vala:233
msgid "GNOME Contacts"
msgstr "Hubungan GNOME"

#: src/contacts-app.vala:234
msgid "About GNOME Contacts"
msgstr "Perihal Hubungan GNOME"

#: src/contacts-app.vala:235
msgid "Contact Management Application"
msgstr "Aplikasi Pengurusan Hubungan"

#: src/contacts-app.vala:236
msgid ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2018 The Contacts Developers"
msgstr ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2018 Para Pembangun Hubungan"

#: src/contacts-app.vala:251
#, c-format
msgid "No contact with email address %s found"
msgstr "Tiada hubungan dengan alamat emel %s ditemui"

#: src/contacts-avatar-selector.vala:111 src/contacts-avatar-selector.vala:231
msgid "Failed to set avatar."
msgstr "Gagal menetapkan avatar."

#: src/contacts-avatar-selector.vala:191
msgid "Browse for more pictures"
msgstr "Layar untuk dapatkan lagi gambar"

#: src/contacts-avatar-selector.vala:194
msgid "_Open"
msgstr "_Buka"

#: src/contacts-avatar-selector.vala:194
msgid "_Cancel"
msgstr "_Batal"

#: src/contacts-contact-editor.vala:27
msgid "Street"
msgstr "Jalan"

#: src/contacts-contact-editor.vala:27
msgid "Extension"
msgstr "Sambungan"

#: src/contacts-contact-editor.vala:27
msgid "City"
msgstr "Bandar"

#: src/contacts-contact-editor.vala:27
msgid "State/Province"
msgstr "Negeri/Daerah"

#: src/contacts-contact-editor.vala:27
msgid "Zip/Postal Code"
msgstr "Zip/Poskod"

#: src/contacts-contact-editor.vala:27
msgid "PO box"
msgstr "PO"

#: src/contacts-contact-editor.vala:27
msgid "Country"
msgstr "Negara"

#: src/contacts-contact-editor.vala:381
msgid "Add email"
msgstr "Tambah emel"

#: src/contacts-contact-editor.vala:383
msgid "Add number"
msgstr "Tambah nombor"

#: src/contacts-contact-editor.vala:387 src/contacts-contact-editor.vala:418
#: src/contacts-contact-editor.vala:452 src/contacts-contact-editor.vala:507
#: src/contacts-contact-editor.vala:557
msgid "Delete field"
msgstr "Padam medan"

#: src/contacts-contact-editor.vala:721 src/contacts-contact-editor.vala:728
#: src/contacts-contact-sheet.vala:254
msgid "Note"
msgstr "Nota"

#: src/contacts-contact-editor.vala:898
msgid "Change avatar"
msgstr "Ubah avatar"

#: src/contacts-contact-editor.vala:940
msgid "Add name"
msgstr "Tambah nama"

#: src/contacts-contact-list.vala:177
msgid "Favorites"
msgstr "Kegemaran"

#: src/contacts-contact-list.vala:179 src/contacts-contact-list.vala:185
msgid "All Contacts"
msgstr "Semua Hubungan"

#: src/contacts-contact-pane.vala:291
msgid "You need to enter some data"
msgstr "Anda perlu memasukkan beberapa data"

#: src/contacts-contact-pane.vala:296
msgid "No primary addressbook configured"
msgstr "Tiada buku alamat utama dikonfigurkan"

#: src/contacts-contact-pane.vala:313
#, c-format
msgid "Unable to create new contacts: %s"
msgstr "Tidak boleh mencipta hubungan baharu: %s"

#: src/contacts-contact-pane.vala:324
msgid "Unable to find newly created contact"
msgstr "Tidak boleh mencari hubungan baharu dicipta"

#: src/contacts-crop-cheese-dialog.vala:91
msgid "Unable to take photo."
msgstr "Tidak dapat menangkap foto."

#: src/contacts-esd-setup.vala:152 src/contacts-esd-setup.vala:167
#: src/contacts-utils.vala:464
msgid "Google"
msgstr "Google"

#: src/contacts-esd-setup.vala:164
msgid "Local Contact"
msgstr "Hubungan Setempat"

#: src/contacts-im-service.vala:32
msgid "AOL Instant Messenger"
msgstr "Pemesejan Segera AOL"

#: src/contacts-im-service.vala:33
msgid "Facebook"
msgstr "Facebook"

#: src/contacts-im-service.vala:34
msgid "Gadu-Gadu"
msgstr "Gadu-Gadu"

#: src/contacts-im-service.vala:35
msgid "Google Talk"
msgstr "Google Talk"

#: src/contacts-im-service.vala:36
msgid "Novell Groupwise"
msgstr "Novell Groupwise"

#: src/contacts-im-service.vala:37
msgid "ICQ"
msgstr "ICQ"

#: src/contacts-im-service.vala:38
msgid "IRC"
msgstr "IRC"

#: src/contacts-im-service.vala:39
msgid "Jabber"
msgstr "Jabber"

#: src/contacts-im-service.vala:40
msgid "Livejournal"
msgstr "Livejournal"

#: src/contacts-im-service.vala:41
msgid "Local network"
msgstr "Rangkaian setempat"

#: src/contacts-im-service.vala:42
msgid "Windows Live Messenger"
msgstr "Windows Live Messenger"

#: src/contacts-im-service.vala:43
msgid "MySpace"
msgstr "MySpace"

#: src/contacts-im-service.vala:44
msgid "MXit"
msgstr "MXit"

#: src/contacts-im-service.vala:45
msgid "Napster"
msgstr "Napster"

#: src/contacts-im-service.vala:46
msgid "Ovi Chat"
msgstr "Ovi Chat"

#: src/contacts-im-service.vala:47
msgid "Tencent QQ"
msgstr "Tencent QQ"

#: src/contacts-im-service.vala:48
msgid "IBM Lotus Sametime"
msgstr "IBM Lotus Sametime"

#: src/contacts-im-service.vala:49
msgid "SILC"
msgstr "SILC"

#: src/contacts-im-service.vala:50
msgid "sip"
msgstr "sip"

#: src/contacts-im-service.vala:51
msgid "Skype"
msgstr "Skype"

#: src/contacts-im-service.vala:52
msgid "Telephony"
msgstr "Telefoni"

#: src/contacts-im-service.vala:53
msgid "Trepia"
msgstr "Trepia"

#: src/contacts-im-service.vala:54 src/contacts-im-service.vala:55
msgid "Yahoo! Messenger"
msgstr "Yahoo! Messenger"

#: src/contacts-im-service.vala:56
msgid "Zephyr"
msgstr "Zephyr"

#: src/contacts-linked-personas-dialog.vala:74
msgid "Unlink"
msgstr "Nyahpaut"

#: src/contacts-link-suggestion-grid.vala:54
#, c-format
msgid "Is this the same person as %s from %s?"
msgstr "Adakah beliau individu yang sama sebagai %s daripada %s?"

#: src/contacts-link-suggestion-grid.vala:57
#, c-format
msgid "Is this the same person as %s?"
msgstr "Adakah beliau individu yang sama sebagai %s?"

#: src/contacts-type-descriptor.vala:85 src/contacts-typeset.vala:261
msgid "Other"
msgstr "Lain-lain"

#. List most specific first, always in upper case
#: src/contacts-typeset.vala:210 src/contacts-typeset.vala:230
#: src/contacts-typeset.vala:256 src/contacts-typeset.vala:258
msgid "Home"
msgstr "Rumah"

#: src/contacts-typeset.vala:211 src/contacts-typeset.vala:231
#: src/contacts-typeset.vala:250 src/contacts-typeset.vala:252
msgid "Work"
msgstr "Kerja"

#. List most specific first, always in upper case
#: src/contacts-typeset.vala:229
msgid "Personal"
msgstr "Peribadi"

#. List most specific first, always in upper case
#: src/contacts-typeset.vala:249
msgid "Assistant"
msgstr "Pembantu"

#: src/contacts-typeset.vala:251
msgid "Work Fax"
msgstr "Faks Kerja"

#: src/contacts-typeset.vala:253
msgid "Callback"
msgstr "Panggil balik"

#: src/contacts-typeset.vala:254
msgid "Car"
msgstr "Kereta"

#: src/contacts-typeset.vala:255
msgid "Company"
msgstr "Syarikat"

#: src/contacts-typeset.vala:257
msgid "Home Fax"
msgstr "Faks Rumah"

#: src/contacts-typeset.vala:259
msgid "ISDN"
msgstr "ISDN"

#: src/contacts-typeset.vala:260
msgid "Mobile"
msgstr "Bimbit"

#: src/contacts-typeset.vala:262
msgid "Fax"
msgstr "Faks"

#: src/contacts-typeset.vala:263
msgid "Pager"
msgstr "Alat Kelui"

#: src/contacts-typeset.vala:264
msgid "Radio"
msgstr "Radio"

#: src/contacts-typeset.vala:265
msgid "Telex"
msgstr "Teleks"

#. To translators: TTY is Teletypewriter
#: src/contacts-typeset.vala:267
msgid "TTY"
msgstr "TTY"

#: src/contacts-utils.vala:462
msgid "Google Circles"
msgstr "Google Circles"

#: src/contacts-window.vala:194
#, c-format
msgid "%d Selected"
msgid_plural "%d Selected"
msgstr[0] "%d Terpilih"
msgstr[1] "%d Terpilih"

#: src/contacts-window.vala:231
msgid "Add"
msgstr "Tambah"

#: src/contacts-window.vala:258
#, c-format
msgid "Editing %s"
msgstr "Menyunting %s"

#: src/contacts-window.vala:314
msgid "Unmark as favorite"
msgstr "Nyahtanda sebagai kegemaran"

#: src/contacts-window.vala:315
msgid "Mark as favorite"
msgstr "Tanda sebagai kegemaran"

#: src/contacts-window.vala:329
msgid "New Contact"
msgstr "Hubungan Baharu"

#: src/contacts-window.vala:427
#, c-format
msgid "%d contacts linked"
msgid_plural "%d contacts linked"
msgstr[0] "%d hubungan dipaut"
msgstr[1] "%d hubungan dipaut"

#: src/contacts-window.vala:431 src/contacts-window.vala:455
#: src/contacts-window.vala:499
msgid "_Undo"
msgstr "_Buat asal"

#: src/contacts-window.vala:450
#, c-format
msgid "Deleted contact %s"
msgstr "%s hubungan dipadamkan"

#: src/contacts-window.vala:452
#, c-format
msgid "%d contact deleted"
msgid_plural "%d contacts deleted"
msgstr[0] "%d hubungan dipadam"
msgstr[1] "%d hubungan dipadam"

#: src/contacts-window.vala:495
#, c-format
msgid "%s linked to %s"
msgstr "%s dipaut kepada %s"

#: src/contacts-window.vala:497
#, c-format
msgid "%s linked to the contact"
msgstr "%s dipaut kepada hubungan"

#: src/org.gnome.Contacts.gschema.xml:6
msgid "First-time setup done."
msgstr "Persediaan pertama-kali selesai."

#: src/org.gnome.Contacts.gschema.xml:7
msgid "Set to true after the user has run the first-time setup wizard."
msgstr ""
"Tetapkan pada benar selepas pengguna menjalankan bestari persediaan pertama-"
"kali."

#: src/org.gnome.Contacts.gschema.xml:11
msgid "Sort contacts on surname."
msgstr "Isih hubungan mengikut nama keluarga."

#: src/org.gnome.Contacts.gschema.xml:12
msgid ""
"If this is set to true, the list of contacts will be sorted on their "
"surnames. Otherwise, it will be sorted on the first names of the contacts."
msgstr ""
"Jika ditetapkan pada benar, senarai hubungan akan diisih mengikut nama "
"keluarga mereka. Jika sebaliknya, ia akan diisih mengikut nama pertama "
"hubungan."

#: src/org.gnome.Contacts.gschema.xml:19
msgid "The default height of the contacts window."
msgstr "Tinggi lalai bagi tetingkap hubungan."

#: src/org.gnome.Contacts.gschema.xml:20
msgid ""
"If the window size has not been changed by the user yet this will be used as "
"the initial value for the height of the window."
msgstr ""
"Jika saiz tetingkap tidak diubah oleh pengguna tetapi akan digunakan sebagai "
"nilai awal untuk tinggi tetingkap."

#: src/org.gnome.Contacts.gschema.xml:26
msgid "The default width of the contacts window."
msgstr "Lebar lalai bagi tetingkap hubungan."

#: src/org.gnome.Contacts.gschema.xml:27
msgid ""
"If the window size has not been changed by the user yet this will be used as "
"the initial value for the width of the window."
msgstr ""
"Jika saiz tetingkap tidak diubah oleh pengguna tetapi akan digunakan sebagai "
"nilai awal untuk lebar tetingkap."

#: src/org.gnome.Contacts.gschema.xml:32
msgid "Is the window maximized?"
msgstr "Adakah tetingkap dimaksimumkan?"

#: src/org.gnome.Contacts.gschema.xml:33
msgid "Stores if the window is currently maximized."
msgstr "Simpan jika tetingkap semasa dimaksimumkan."
